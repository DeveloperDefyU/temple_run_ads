﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.SceneManagement;

public class CharacterManager : MonoBehaviour
{
    public static CharacterManager Instance;

    public static readonly string CURRENT_CHARACTER_KEY = "SGLIB_CURRENT_CHARACTER";

    public bool Chosen;

    public bool AllLoaded;

    private void Start()
    {
       // SetCharaters(new int[] { 1, 2,4 });
    }

    public GameObject SelectedCharacter { get { return characters[CurrentCharacterIndex]; } }

    public int CurrentCharacterIndex
    {
        get
        {
            return PlayerPrefs.GetInt(CURRENT_CHARACTER_KEY, 0);
        }
        set
        {
            PlayerPrefs.SetInt(CURRENT_CHARACTER_KEY, value);
            PlayerPrefs.Save();
            Chosen = true;
        }
    }

    public GameObject[] characters;
    public GameObject[] PossibleCharacters;

    void Awake()
    {
        if (Instance)
        {
            DestroyImmediate(gameObject);
        }
        else
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
    }

    public void SetCharaters(int[] selectedCharaters)
    {
        characters = new GameObject[selectedCharaters.Length];
        for (int i = 0; i < selectedCharaters.Length; i++)
        {
            characters[i] = PossibleCharacters[selectedCharaters[i]];
        }

       FindObjectOfType< CharacterScroller>().DoStart();
    }

    internal void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name, LoadSceneMode.Single);
    }
}

