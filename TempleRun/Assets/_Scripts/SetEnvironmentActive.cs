﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetEnvironmentActive : MonoBehaviour {

    public int EnvironmentID;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Set(int envi)
    {
        gameObject.SetActive(EnvironmentID == envi);
    }
}
