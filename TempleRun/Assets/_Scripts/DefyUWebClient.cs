﻿

using System.Collections.Generic;
using System.Net;
using System.Threading;
using UnityEngine;
using System;
using System.Linq;

public partial class DefyUWebClient : MonoBehaviour
{
    [System.NonSerialized]

    public string productionDomain = @"http://api.defyu.com";
    public string productionDomain_RealTime = @"http://realtime.defyu.com";
    public string productionBaseUrl = "api";
#if UNITY_EDITOR
#if !BACKEND
    public string developmentDomain = @"http://api-dev.defyu.com";
    public string developmentDomain_RealTime = @"http://realtime-dev.defyu.com";    
#else
    public string developmentDomain = @"http://localhost:50288";
    public string developmentDomain_RealTime = @"http://realtime-dev.defyu.com";
#endif
    public string developmentBaseUrl = "api";
#endif
#if UNITY_EDITOR
    public bool useProduction = true;
#endif

    private static DefyUWebClient _instance;
    private static DefyUWebClient instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<DefyUWebClient>();

                if (_instance == null)
                {
                    GameObject go = new GameObject();
                    go.name = "DefyUWebClient";
                    _instance = go.AddComponent<DefyUWebClient>();
                }
            }

            return _instance;
        }
    }

    private string _baseUrl
    {
        get
        {
#if UNITY_EDITOR 
            if (!useProduction)
                return string.Format("{0}/{1}", developmentDomain, developmentBaseUrl);
            else
                return string.Format("{0}/{1}", productionDomain, productionBaseUrl);
#else
            return string.Format("{0}/{1}", productionDomain, productionBaseUrl);
#endif
        }
    }
    private string _domain
    {
        get
        {
#if UNITY_EDITOR
            if (!useProduction)
                return developmentDomain;
            else
                return productionDomain;
#else
            return productionDomain;
#endif
        }
    }

    private string _domain_realTime
    {
        get
        {
#if UNITY_EDITOR
            if (!useProduction)
                return developmentDomain_RealTime;
            else
                return productionDomain_RealTime; ;
#else
            return productionDomain_RealTime;
#endif
        }
    }

    [System.NonSerialized]
    static string _deviceId = string.Empty;

    public static string deviceId
    {
        get
        {
            if (string.IsNullOrEmpty(_deviceId))
            {
                _deviceId = SystemInfo.deviceUniqueIdentifier;
            }

            return _deviceId;
        }
    }
    public static string domain { get { return instance._domain; } }
    public static string baseUrl { get { return instance._baseUrl; } }
    public static string appVersion
    {
        get
        {
            return Application.version;
        }
    }

    int millisRequestTimeout;
    private float requestTimeout
    {
        get
        {
#if UNITY_EDITOR
            return 25f;
#else
            return 25.0f;
#endif
        }
    }


    


    public delegate void ClientResponseCallback<T>(bool success, T entity);
    delegate void ServerResponseCallback<T>(bool success, string json, ClientResponseCallback<T> clientCallback);
    public delegate void ClientResponse<T>(bool success, T entity);
    delegate void ResponseCallback(bool success, string json);
    public delegate void ComunicationCallback();
    public static event ComunicationCallback onSendRequest;
    public static event ComunicationCallback onReceiveResponses;
   


    const string PP_KEY_ACCESS_TOKEN = "AccessToken";
    const string APP_ID = "LqmtIewdZH";
    public static bool hasToken { get { return PlayerPrefs.HasKey(PP_KEY_ACCESS_TOKEN); ; } }

    [System.NonSerialized]
    static string _accessToken = string.Empty;
    public static string accessToken
    {
        get
        {
            if (string.IsNullOrEmpty(_accessToken))
            {
                _accessToken = PlayerPrefs.GetString(PP_KEY_ACCESS_TOKEN, string.Empty);
            }

            return _accessToken;
        }
    }

    [System.NonSerialized]
    static string _messagingToken = string.Empty;
  


    void Awake()
    {
        DontDestroyOnLoad(this);
        millisRequestTimeout = (int)(1000 * requestTimeout);
    }

    void Update()
    {
        TriggerEnqueuedActions();
    }

    #region Requests

    

    public static void Request_Image(string gameId, int imageId, ClientResponseCallback<string> callback)
    {
        switch (imageId)
        {

            case 0:
                callback(true, "https://www.freepnglogos.com/uploads/puma-logo-png-6.png");
                break;

            case 7:
                callback(true, "https://cdn.runrepeat.com/i/puma/23198/puma-men-s-ignite-evoknit-cross-trainer-shoe-puma-black-quiet-shade-puma-white-7-m-us-mens-puma-black-quiet-shade-puma-white-d00-600.jpg");
                break;

        }
       
    }

    public static void Request_Text(string gameId, int imageId, ClientResponseCallback<string> callback)
    {
        switch (imageId) {

            case 1:
            callback(true, "JAVI");
                break;

            case 2:
                callback(true, "PUMA ESCAPE");
                break;
            case 5:
                callback(true, "30% Discount");
                break;
            case 6:
                callback(true, "http://puma.com");
                break;

        }
    }

    public static void Request_Option(string gameId, int imageId, ClientResponseCallback<int> callback)
    {

        switch (imageId)
        {

            case 3:
                callback(true, 0);
                break;

            case 4:
                callback(true, 5);
                break;

        }
        
    }



    #endregion


    #region Thread

    delegate void QueueAction();
    static Queue<QueueAction> queue = new Queue<QueueAction>();

    enum RequestMethod
    {
        GET,
        POST
    }

    void TriggerEnqueuedActions()
    {
        if (queue.Count > 0)
        {
            while (queue.Count > 0)
            {
                //  System.Threading.Thread.Sleep(2000);
                QueueAction action = queue.Dequeue();
                action();

            }

            if (onReceiveResponses != null)
                onReceiveResponses();
        }
    }

    void Request(string url)
    {
#if UNITY_EDITOR
        Debug.LogFormat("[Url = {0}]", url);
#endif
        Thread t;
        string auxDeviceId = deviceId;
        string auxAccessToken = accessToken;
        t = new Thread(() => ThreadedRequest(url));
        t.Start();
    }

    void Request<T>(string url, ServerResponseCallback<T> serverCallback, ClientResponseCallback<T> clientCallback, object args = null, RequestMethod? method = null, bool addRequestTimeout = true, bool sendRequestEvent = true)
    {
#if UNITY_EDITOR
        Debug.LogFormat("[Url = {0}]", url);
#endif
        Thread t;
        string auxDeviceId = deviceId;
        string auxAccessToken = accessToken;
        t = new Thread(() => ThreadedRequest(url, serverCallback, clientCallback, auxDeviceId, auxAccessToken, args, method));

        

        if (sendRequestEvent && onSendRequest != null)
            onSendRequest();

        t.Start();
    }

    void ThreadedRequest(string url)
    {
        using (WebClient client = new WebClient())
        {
            client.Encoding = System.Text.Encoding.UTF8;
            client.Headers.Add("app-id", APP_ID);
            client.Headers.Add("device-id", deviceId);

            string json = "{}";
            json = client.DownloadString(url);
        }
    }

    void ThreadedRequest<T>(string url, ServerResponseCallback<T> serverCallback, ClientResponseCallback<T> clientCallback, string deviceId, string accessToken, object args = null, RequestMethod? method = null)
    {
        using (WebClient client = new WebClient())
        {
            client.Encoding = System.Text.Encoding.UTF8;
            client.Headers.Add("app-id", APP_ID);
            client.Headers.Add("device-id", deviceId);
            client.Headers.Add("access-token", accessToken);

            string json = "{}";
            if (args == null || !method.HasValue || method != RequestMethod.POST)
            {
                json = client.DownloadString(url);
            }
            else if (args != null && method.HasValue && method == RequestMethod.POST)
            {
                client.Headers.Add(HttpRequestHeader.ContentType, "application/json");
                string json_args = JsonUtility.ToJson(args);
                json = client.UploadString(url, (method == RequestMethod.POST) ? "POST" : "GET", json_args);
            }

            EnqueueAction(serverCallback, clientCallback, true, json);
        }
    }

    void EnqueueAction<T>(ServerResponseCallback<T> serverCallback, ClientResponseCallback<T> clientCallback, bool success, string json)
    {
        lock (queue)
        {
            queue.Enqueue(() =>
            {
#if UNITY_EDITOR
                Debug.LogFormat("Response [Json = {0}]", json);
#endif
                serverCallback(true, json, clientCallback);
            });
        }
    }

    void EnqueueAction(ResponseCallback callback, bool success, string json, long serverTimeInSeconds = 0, long expirationTime = 0)
    {
        lock (queue)
        {
            queue.Enqueue(() =>
            {
#if UNITY_EDITOR
                Debug.LogFormat("Response [Json = {0}]", json);
#endif
                callback(true, json);
            });
        }
    }

   

   

    #endregion
}