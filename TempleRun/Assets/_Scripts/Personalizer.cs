﻿using FullInspector;
using LlockhamIndustries.Decals;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class Personalizer : BaseBehavior {

    #region singleton
    private static Personalizer _instance;
    public static Personalizer Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<Personalizer>();

                if (_instance == null)
                {
                    GameObject go = new GameObject();
                    go.name = "Personalizer";
                    _instance = go.AddComponent<Personalizer>();
                }
            }

            return _instance;
        }
    }

   


    #endregion

    public string GameID;
    public Dictionary<Identifiers, Material> MaterialMain;
    public Dictionary<Identifiers, ProjectionRenderer[]> Decals;

    public Dictionary<Identifiers, TextMeshPro[]> TextMeshes;
    public Dictionary<Identifiers, TextMeshProUGUI> TextMeshesUGUI;
    public Dictionary<Identifiers, Image> Images;

   // public Dictionary<Identifiers, ISetter> VaribleInt;
    public Dictionary<Identifiers, ISetterString> VariableString;
    public Dictionary<Identifiers, ISetterString> VariableSprite;

    public Dictionary<Identifiers, ProductSetter> VariableProduct;

    private Dictionary<Identifiers, RendererObserver[]> RendererObservers;

    private Dictionary<Identifiers, float> VisiualizationTimes;

    private bool _optionsSet;

    public bool AllDone;

    // Use this for initialization
    void Start () {
        // DontDestroyOnLoad(gameObject);
        StartCoroutine(AskDelayed());


    }

    private IEnumerator AskDelayed()
    {
      if (CharacterManager.Instance.AllLoaded)
            yield break;
        AllDone = false;
        yield return new WaitForEndOfFrame();
        //AskForOptions();
        // StartCoroutine(SecureOptions());
        yield return new WaitForEndOfFrame();
      //  AskForMaterialsMain();
        yield return new WaitForEndOfFrame();
        AskForDecals();
        yield return new WaitForEndOfFrame();
        AskForTextMeshes();
        yield return new WaitForEndOfFrame();
        AskForTextMeshesUGUI();
        yield return new WaitForEndOfFrame();
        AskForStrings();
        yield return new WaitForEndOfFrame();
        AskForImages();
        yield return new WaitForEndOfFrame();
        AskForSprites();
    }

    //private IEnumerator SecureOptions()
    //{
    //    yield return new WaitForSeconds(2);
    //    if (!_optionsSet)
    //    {
    //        _optionsSet = true;
    //        VaribleInt[Identifiers.Environment].SetValue(0);
    //    }
            
    //}

    // Update is called once per frame
    void Update () {
		
	}

    #region ask for stuff
    private void AskForMaterialsMain()
    {
        foreach(var mm in MaterialMain)
        {
            AskForMaterialMain(((int)(mm.Key)).ToString(), mm.Value);
            SetRenderes(mm.Key);
        }
    }

    private void AskForDecals()
    {
        foreach (var mm in Decals)
        {
            AskForDecal(((int)(mm.Key)).ToString(), mm.Value);
            SetRenderesDecals(mm.Key);
        }
    }

    private void AskForTextMeshes()
    {
        foreach (var tm in TextMeshes)
        {
            AskForText(((int)(tm.Key)).ToString(), tm.Value);
            SetRenderesTextMesh(tm.Key);
        }
    }

    private void AskForTextMeshesUGUI()
    {
        foreach (var tm in TextMeshesUGUI)
        {
            AskForTextUGUI(((int)(tm.Key)).ToString(), tm.Value);
            SetRenderesTextMeshUGUI(tm.Key);
        }
    }

    //private void AskForOptions()
    //{
    //    foreach (var tm in VaribleInt)
    //    {
    //        AskForOptions(((int)(tm.Key)).ToString(), tm.Value);
    //    }
    //}

    private void AskForStrings()
    {
        foreach (var tm in VariableString)
        {
            string s = tm.Key.ToString();

            try
            {
                int i = int.Parse(s.Split("_"[0])[1]) - 1;

                AskForString(((int)(tm.Key)).ToString(), tm.Value, i);
            }
            catch (IndexOutOfRangeException e)
            {
                Debug.Log(" ask catch");
                AskForString(((int)(tm.Key)).ToString(), tm.Value);
            }
            
        }
    }

    private void AskForImages()
    {
        foreach (var tm in Images)
        {
            AskForImage(((int)(tm.Key)).ToString(), tm.Value);
            SetRenderesImages(tm.Key);
        }
    }

    private void AskForSprites()
    {
        foreach (var tm in VariableSprite)
        {
            string s = tm.Key.ToString();

            try
            {
                int i = int.Parse(s.Split("_"[0])[1]) -1 ;

                AskForSprite(((int)(tm.Key)).ToString(), tm.Value, i);
                SgLib.InAppPurchaser.Instance.coinPacks[i].Identifier = tm.Key;
               // System.Threading.Thread.Sleep(1000);
            }
            catch(IndexOutOfRangeException e )
            {
                AskForSprite(((int)(tm.Key)).ToString(), tm.Value);
            }
            
        }
    }

    #endregion



    #region server communication

    private void AskForMaterialMain(string id, Material material)
    {
        WebApiClient.Request_Image( id,
             (bool success, string result) =>
             {
                 if (success)
                 {
                     ResponseMainMaterial(result, material);
                 }

             });


    }

    private void ResponseMainMaterial(string result, Material material)
    {

        StartCoroutine(SetMaterialMainTexture(result, material));

    }

    private void AskForDecal(string id, ProjectionRenderer[] decal)
    {
        WebApiClient.Request_Image(id,
             (bool success, string result) =>
             {
                 if (success)
                 {
                     ResponseDecal(result, decal);
                 }

             });


    }

    private void ResponseDecal(string result, ProjectionRenderer[] decal)
    {

        StartCoroutine(SetDecal(result, decal));

    }

    private void AskForImage(string id, Image image)
    {
        WebApiClient.Request_Image( id,
             (bool success, string result) =>
             {
                 if (success)
                 {
                     ResponseImage(result, image);
                 }

             });


    }

    private void ResponseImage(string result, Image image)
    {

        StartCoroutine(SetImageTexture(result, image));

    }

    private void AskForText(string id, TextMeshPro[] textMesh)
    {
        WebApiClient.Request_Text( id,
             (bool success, string result) =>
             {
                 if (success)
                 {
                     ResponseGetText(result, textMesh);
                 }

             });


    }

    private void ResponseGetText(string result, TextMeshPro[] textMeshes)
    {
        foreach (var textMesh in textMeshes)
        {
            textMesh.text = result;
        }
       

    }

    private void AskForTextUGUI(string id, TextMeshProUGUI textMesh)
    {
        WebApiClient.Request_Text( id,
             (bool success, string result) =>
             {
                 if (success)
                 {
                     ResponseGetTextUGUI(result, textMesh);
                 }

             });


    }

    private void ResponseGetTextUGUI(string result, TextMeshProUGUI textMesh)
    {

        textMesh.text = result;

    }

    private void AskForOptions(string id, ISetter setter)
    {
        WebApiClient.Request_Numeric( id,
             (bool success, float result) =>
             {
                 if (success)
                 {
                    if(id == "8")
                     {
                         if (_optionsSet)
                             return;
                         _optionsSet = true;
                     }
                    
                     ResponseOptions(result, setter);
                 }

             });


    }

    private void ResponseOptions(float result, ISetter setter)
    {
     
        setter.SetValue((int)result);

    }

    private void AskForString(string id, ISetterString setter, int i=0)
    {
        WebApiClient.Request_Text( id,
             (bool success, string result) =>
             {
                 if (success)
                 {
                     ResponseString(result, setter, i);
                 }

             });


    }

    private void ResponseString(string result, ISetterString setter, int i)
    {

        setter.SetValue(result,i);

    }

    private void AskForSprite(string id, ISetterString setter, int i=0)
    {
        WebApiClient.Request_Image(id,
             (bool success, string result) =>
             {
                 if (success)
                 {
                     ResponseSprite(result, setter,i);
                 }

             });


    }

    private void ResponseSprite(string result, ISetterString setter, int i)
    {

        setter.SetValue(result,i);

      
           

    }



    #endregion

    #region servercomunication analyitics
    private void SendVisible(string id)
    {
        WebApiClient.Request_NotifyShown(id);

    }

    private void SendInvisible(string id, int timePassed)
    {
        WebApiClient.Request_NotifyHidden(id, timePassed);

    }

    private void SendClick(string id)
    {
         WebApiClient.Request_NotifyClick(id);

    }


    #endregion


    #region www

    private IEnumerator SetMaterialMainTexture(string url,  Material material)
    {
        
        var www = UnityWebRequestTexture.GetTexture(url);
        yield return www.SendWebRequest();

        if (www.isNetworkError)
        {
           // SetDefaultImage(defaultImage);

            www.Dispose();
            www = null;
            yield break;
        }
        else
        {
            try
            {
                var texture = ((DownloadHandlerTexture)www.downloadHandler).texture;


                material.mainTexture = texture;

                var rends = FindRenderers(material);
                foreach (var ren in rends)
                {
                    ren.material.mainTexture = texture;
                }


                www.Dispose();
                www = null;

               
            }
            catch (InvalidOperationException ex)
            {
                
            }
        }
    }

    private IEnumerator SetDecal(string url, ProjectionRenderer[] decals)
    {

        var www = UnityWebRequestTexture.GetTexture(url);
        yield return www.SendWebRequest();

        if (www.isNetworkError)
        {
            // SetDefaultImage(defaultImage);

            www.Dispose();
            www = null;
            yield break;
        }
        else
        {
            try
            {
                var texture = ((DownloadHandlerTexture)www.downloadHandler).texture;

                foreach (var decal in decals)
                {
                    decal.Renderer.material.mainTexture = texture;
                }
               

                //var rends = FindRenderers(material);
                //foreach (var ren in rends)
                //{
                //    ren.material.mainTexture = texture;
                //}


                www.Dispose();
                www = null;


            }
            catch (InvalidOperationException ex)
            {

            }
        }
    }
    private IEnumerator SetImageTexture(string url, Image image)
    {

        var www = UnityWebRequestTexture.GetTexture(url);
        yield return www.SendWebRequest();

        if (www.isNetworkError)
        {
            // SetDefaultImage(defaultImage);

            www.Dispose();
            www = null;
            yield break;
        }
        else
        {
            try
            {
                var texture = ((DownloadHandlerTexture)www.downloadHandler).texture;

                texture = ((DownloadHandlerTexture)www.downloadHandler).texture;
                Rect rec = new Rect(0, 0, 0, 0);

                rec.width = texture.width;
                rec.height = texture.height;

                image.sprite = Sprite.Create(texture, rec, new Vector2(0.5f, 0.5f)); // , 100;

                www.Dispose();
                www = null;

                AllDone = true;
               CharacterManager.Instance.AllLoaded = true;
            }
            catch (InvalidOperationException ex)
            {

            }
        }
    }

    #endregion

    #region set for analytics
    void SetRenderes(Identifiers matId)
    {
        if (RendererObservers == null)
            RendererObservers = new Dictionary<Identifiers, RendererObserver[]>();

        var rends = FindRenderers(matId);
        if (RendererObservers.ContainsKey(matId))
            RendererObservers[matId] = rends;
        else
            RendererObservers.Add(matId, rends);

    }

    void SetRenderesDecals(Identifiers matId)
    {
        if (RendererObservers == null)
            RendererObservers = new Dictionary<Identifiers, RendererObserver[]>();

        var rends = FindRenderersDecal(matId);
        if (RendererObservers.ContainsKey(matId))
            RendererObservers[matId] = rends;
        else
            RendererObservers.Add(matId, rends);

    }

    void SetRenderesTextMesh(Identifiers matId)
    {
        if (RendererObservers == null)
            RendererObservers = new Dictionary<Identifiers, RendererObserver[]>();

        var rend = TextMeshes[matId];
        List<RendererObserver> tempList = new List<RendererObserver>();
        foreach (var item in rend)
        {
            var observer = item.gameObject.AddComponent<RendererObserver>();
            observer.Init(matId);
            tempList.Add(observer);
        }
       
       
      
        if (RendererObservers.ContainsKey(matId))
            RendererObservers[matId] = tempList.ToArray();
        else
            RendererObservers.Add(matId, tempList.ToArray());
    }

    void SetRenderesTextMeshUGUI(Identifiers matId)
    {
        if (RendererObservers == null)
            RendererObservers = new Dictionary<Identifiers, RendererObserver[]>();

        var rend = TextMeshesUGUI[matId];
        var observer = rend.gameObject.AddComponent<RendererObserver>();
        observer.Init(matId);


        if (RendererObservers.ContainsKey(matId))
            RendererObservers[matId] = new RendererObserver[] { observer };
        else
            RendererObservers.Add(matId, new RendererObserver[] { observer });
    }

    void SetRenderesImages(Identifiers matId)
    {
        if (RendererObservers == null)
            RendererObservers = new Dictionary<Identifiers, RendererObserver[]>();

        var rend = Images[matId];
        var observer = rend.gameObject.AddComponent<RendererObserver>();
        observer.Init(matId);


        if (RendererObservers.ContainsKey(matId))
            RendererObservers[matId] = new RendererObserver[] { observer };
        else
            RendererObservers.Add(matId, new RendererObserver[] { observer });
    }

    private RendererObserver[] FindRenderersDecal(Identifiers matId)
    {
        var mat = Decals[matId];
       
        List<RendererObserver> rendWithMat = new List<RendererObserver>();
        foreach (var rend in mat)
        {
           
                var observer = rend.gameObject.AddComponent<RendererObserver>();
                observer.Init(matId);
                rendWithMat.Add(observer);
           
               
        }
        return rendWithMat.ToArray();
    }

    private RendererObserver[] FindRenderers(Identifiers matId)
    {
        Material mat = MaterialMain[matId];
        var rends = FindObjectsOfType<Renderer>();
        List<RendererObserver> rendWithMat = new List<RendererObserver>();
        foreach (var rend in rends)
        {
            if (rend.material == mat || rend.material.name.StartsWith(mat.name))
            {
                var observer = rend.gameObject.AddComponent<RendererObserver>();
                observer.Init(matId);
                rendWithMat.Add(observer);
            }

        }
        return rendWithMat.ToArray();
    }

    private Renderer[] FindRenderers(Material mat)
    {
        
        var rends = FindObjectsOfType<Renderer>();
        List<Renderer> rendWithMat = new List<Renderer>();
        foreach (var rend in rends)
        {
            if (rend.material == mat || rend.material.name.StartsWith(mat.name))
            {
               
                rendWithMat.Add(rend);
            }

        }
        return rendWithMat.ToArray();
    }



    #endregion

    #region analyitics events
    public static void RenderVisible(Identifiers rendererId)
    {
        if (GameManager.Instance.GameState == GameState.Playing)
            Instance._RenderVisible(rendererId);
    }

    private void _RenderVisible(Identifiers rendererId)
    {
       if(!RendererObservers[rendererId].Any( x => x.IsVisible))
        {
            if (GameManager.Instance.GameState == GameState.Playing)
            {
                SendVisible(((int)rendererId).ToString());
                SetVisibleStart(rendererId);
            }
               
        }
    }

    private void SetVisibleStart(Identifiers rendererId)
    {
        if (VisiualizationTimes == null)
            VisiualizationTimes = new Dictionary<Identifiers, float>();

        if (!VisiualizationTimes.ContainsKey(rendererId))
        {
            VisiualizationTimes.Add(rendererId, 0);
        }

        VisiualizationTimes[rendererId] = DateTime.Now.Millisecond;
    }

    internal static void RenderInvisible(Identifiers rendererId)
    {
        Instance._RenderInvisible(rendererId);
    }

    private void _RenderInvisible(Identifiers rendererId)
    {
        if (GameManager.Instance == null || GameManager.Instance.GameState != GameState.Playing)
            return;

            if (RendererObservers == null || RendererObservers[rendererId].Length == 0)
        {
            Debug.Log(rendererId + " no renderer found");
        }
       
        if (!RendererObservers[rendererId].Any(x => x.IsVisible))
        {
            if (VisiualizationTimes == null || !VisiualizationTimes.ContainsKey(rendererId) || VisiualizationTimes[rendererId]<=0)
            {
                Debug.LogError("visible end without visible start");
                return;
            }

            var timePassed = DateTime.Now.Millisecond - VisiualizationTimes[rendererId];
           
                SendInvisible(((int)rendererId).ToString(), (int)timePassed);
        }
    }

    public static void SendClickEvent(Identifiers id)
    {
        Instance._SendClickEvent(id);
    }

    private void _SendClickEvent(Identifiers id)
    {        
        if(GameManager.Instance.GameState == GameState.Playing)
            SendClick(((int)id).ToString());
    }

    #endregion

}




public enum Identifiers
{
    BallDecalMain =1, //url texture
    BallName, //string
    GameName, //string
    RewardText, //string
    RewardImage, //url texture
    RewardUrl, //url
    RewardCoinsNeeded, // int
    Environment, //int
    

    ProductPrice_1, //string
    ProductPrice_2, //string
    ProductPrice_3, //string
    ProductPrice_4, //string
    ProductPrice_5, //string
    ProductPrice_6, //string

    ProductImage_1, //url texture
    ProductImage_2, //url texture
    ProductImage_3, //url texture
    ProductImage_4, //url texture
    ProductImage_5, //url texture
    ProductImage_6, //url texture

    ProductUrl_1, //url 
    ProductUrl_2, //url 
    ProductUrl_3, //url 
    ProductUrl_4, //url 
    ProductUrl_5, //url 
    ProductUrl_6, //url 

    Character, //int


}