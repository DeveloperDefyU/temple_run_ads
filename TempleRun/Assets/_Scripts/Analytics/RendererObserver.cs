﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RendererObserver : MonoBehaviour {

    private Identifiers _rendererId;
    private bool _isVisible;

    public bool IsVisible{get{return _isVisible;}}

    private RectTransform _rectTransform;
    private bool _isUGUI;

    public void Init(Identifiers renderer)
    {
        _rendererId = renderer;
        if (IsUGUI())
        {
            SetForUGUI();
        }
    }

    void OnBecameVisible()
    {
        Personalizer.RenderVisible(_rendererId);
        _isVisible = true;
    }

    void OnBecameInvisible()
    {
        _isVisible = false;
        Personalizer.RenderInvisible(_rendererId);
        
    }

    private void OnDisable()
    {
      //  if (_isVisible)
         //   OnBecameInvisible();
    }


    private void Update()
    {
        if (_isUGUI)
            CheckVisibleUGUI();
    }

    private void CheckVisibleUGUI()
    {
        bool isVisible = _rectTransform.ActiveAndEnabled();
        if (!_isVisible && isVisible)
            OnBecameVisible();
        if (_isVisible && !isVisible)
            OnBecameInvisible();
    }

    private bool IsUGUI()
    {
        return GetComponent<CanvasRenderer>() != null;
    }

    private void SetForUGUI()
    {
        _rectTransform = GetComponent<RectTransform>();
        _isUGUI = true;
       
    }
}
