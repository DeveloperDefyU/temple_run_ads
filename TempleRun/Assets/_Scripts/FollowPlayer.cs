﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayer : MonoBehaviour {

    public Transform Player;

    private Vector3 _offset;
    private float fixedY;
	// Use this for initialization
	void Start () {
        _offset = transform.position - Player.position;
        fixedY = transform.position.y;
    }
	
	// Update is called once per frame
	void Update () {
        Vector3 pos = Player.position + _offset;
        pos.y = fixedY;
        transform.position = pos;

    }
}
