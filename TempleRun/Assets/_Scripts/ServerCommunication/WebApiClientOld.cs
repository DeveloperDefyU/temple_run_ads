﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using UnityEngine;

public class WebApiClientOld : MonoBehaviour
{
    [System.NonSerialized]
    public string domain = @"http://ads.defyu.com";
    public string baseUrl { get { return string.Format(@"{0}/api", domain); } }

    private static WebApiClientOld _instance;
    private static WebApiClientOld instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<WebApiClientOld>();

                if (_instance == null)
                {
                    GameObject go = new GameObject();
                    go.name = "WebApiClient";
                    _instance = go.AddComponent<WebApiClientOld>();
                }
            }

            return _instance;
        }
    }

    public delegate void ClientResponseCallback<T>(bool success, T entity);
    delegate void ServerResponseCallback<T>(bool success, string json, ClientResponseCallback<T> clientCallback);
    public delegate void ClientResponse<T>(bool success, T entity);
    delegate void ResponseCallback(bool success, string json);

#if OFUSCATE
    const string COMMAND_IMAGE = "i";
    const string COMMAND_TEXT = "t";
    const string COMMAND_OPTION = "o";
#else
    const string COMMAND_IMAGE = "image";
    const string COMMAND_TEXT = "text";
    const string COMMAND_OPTION = "option";
#endif

    void Awake()
    {
        //DontDestroyOnLoad(this);
    }

    void Update()
    {
        TriggerEnqueuedActions();
    }

    public static void Request_Image(string key, ClientResponseCallback<string> callback)
    {
        switch (key)
        {
            case "ProductImage_1":               
                callback(true, "https://images-na.ssl-images-amazon.com/images/I/41fwP7YrqqL._AC_US200_.jpg");
                return;
            case "ProductImage_2":
                callback(true, "https://images-na.ssl-images-amazon.com/images/I/3191zc769YL._AC_US200_.jpg");
                return;
            case "ProductImage_3":
                callback(true, "https://images-na.ssl-images-amazon.com/images/I/51WubH34v5L._AC_US200_.jpg");
                return;
            case "ProductImage_4":
                callback(true, "https://images-na.ssl-images-amazon.com/images/I/41GvGq2vNHL._AC_US200_.jpg");
                return;
            case "ProductImage_5":
                callback(true, "https://images-na.ssl-images-amazon.com/images/I/51IyRKRJkPL._AC_US200_.jpg");
                return;
            case "ProductImage_6":
                callback(true, "https://images-na.ssl-images-amazon.com/images/I/41m8ts2XZ4L._AC_US200_.jpg");
                return;

            
        }

        string url = string.Format("{0}/{1}/{2}", instance.baseUrl, COMMAND_IMAGE, key);
        instance.Request(url, instance.ImageServerCallback, callback);
    }

    public static void Request_Text(string key, ClientResponseCallback<string> callback)
    {
        switch (key)
        {
            case "ProductPrice_1":
                callback(true, "56.69$");
                return;
            case "ProductPrice_2":
                callback(true, "8.00$");
                return;
            case "ProductPrice_3":
                callback(true, "13.49$");
                return;
            case "ProductPrice_4":
                callback(true, "25.22$");
                return;
            case "ProductPrice_5":
                callback(true, "20.99$");
                return;
            case "ProductPrice_6":
                callback(true, "41.20$");
                return;

            case "ProductUrl_1":
                callback(true, "https://www.amazon.com/PUMA-Smash-Sneaker-Peacoat-White/dp/B0721LTNKQ/ref=sr_1_1_sspa?ie=UTF8&qid=1520436666&sr=8-1-spons&keywords=puma&psc=1");
                return;
            case "ProductUrl_2":
                callback(true, "https://www.amazon.com/PUMA-Boys-White-10-12-Medium/dp/B00QYCHMLS/ref=sr_1_9?ie=UTF8&qid=1520436666&sr=8-9&keywords=puma");
                return;
            case "ProductUrl_3":
                callback(true, "https://www.amazon.com/PUMA-Socks-Mens-Grey-Black/dp/B012AP0LVA/ref=sr_1_28_sspa?ie=UTF8&qid=1520436666&sr=8-28-spons&keywords=puma&psc=1");
                return;
            case "ProductUrl_4":
                callback(true, "https://www.amazon.com/PUMA-Womens-Archive-Jacket-Depths/dp/B01N49Q3EZ/ref=sr_1_40?ie=UTF8&qid=1520436666&sr=8-40&keywords=puma");
                return;
            case "ProductUrl_5":
                callback(true, "https://www.amazon.com/PUMA-Little-Combo-Accessory-Youth/dp/B073446YC7/ref=sr_1_53?ie=UTF8&qid=1520436666&sr=8-53&keywords=puma");
                return;
            case "ProductUrl_6":
                callback(true, "https://www.amazon.com/PUMA-Tsugi-Shinsei-Sneaker-Olive/dp/B01MXYZ4RS/ref=sr_1_43?ie=UTF8&qid=1520436666&sr=8-43&keywords=puma");
                return;

            case "RewardUrl":
                callback(true, "https://www.realmadrid.com/en");
                return;



        }
        string url = string.Format("{0}/{1}/{2}", instance.baseUrl, COMMAND_TEXT, key);
        instance.Request(url, instance.TextServerCallback, callback);
    }

    public static void Request_Option(string key, ClientResponseCallback<float> callback)
    {
        string url = string.Format("{0}/{1}/{2}", instance.baseUrl, COMMAND_OPTION, key);
        instance.Request(url, instance.OptionServerCallback, callback);
    }

    void ImageServerCallback(bool success, string json, ClientResponseCallback<string> clientCallback)
    {
        if (clientCallback != null)
        {
            var entity = JsonUtility.FromJson<Response>(json);
            clientCallback(success, entity.Image);
        }
    }

    void TextServerCallback(bool success, string json, ClientResponseCallback<string> clientCallback)
    {
        if (clientCallback != null)
        {
            var entity = JsonUtility.FromJson<Response>(json);
            clientCallback(success, entity.Text);
        }
    }

    void OptionServerCallback(bool success, string json, ClientResponseCallback<float> clientCallback)
    {
        if (clientCallback != null)
        {
            var entity = JsonUtility.FromJson<Response>(json);
            clientCallback(success, entity.Numeric);
        }
    }

    #region Thread

    delegate void QueueAction();
    static Queue<QueueAction> queue = new Queue<QueueAction>();

    enum RequestMethod
    {
        GET,
        POST
    }

    void TriggerEnqueuedActions()
    {
        if (queue.Count > 0)
        {
            while (queue.Count > 0)
            {
                QueueAction action = queue.Dequeue();
                action();
            }
        }
    }

    void Request(string url)
    {
#if UNITY_EDITOR
        Debug.LogFormat("[Url = {0}]", url);
#endif
        Thread t;
        t = new Thread(() => ThreadedRequest(url));
        t.Start();
    }

    void Request<T>(string url, ServerResponseCallback<T> serverCallback, ClientResponseCallback<T> clientCallback, object args = null, RequestMethod? method = null, bool addRequestTimeout = true, bool sendRequestEvent = true)
    {
#if UNITY_EDITOR
        Debug.LogFormat("[Url = {0}]", url);
#endif
        Thread t;
        t = new Thread(() => ThreadedRequest(url, serverCallback, clientCallback, args, method));

        t.Start();
    }

    void ThreadedRequest(string url)
    {
        using (WebClient client = new WebClient())
        {
            client.Encoding = System.Text.Encoding.UTF8;

            string json = "{}";
            json = client.DownloadString(url);
        }
    }

    void ThreadedRequest<T>(string url, ServerResponseCallback<T> serverCallback, ClientResponseCallback<T> clientCallback, object args = null, RequestMethod? method = null)
    {
        using (WebClient client = new WebClient())
        {
            client.Encoding = System.Text.Encoding.UTF8;

            string json = "{}";
            if (args == null || !method.HasValue || method != RequestMethod.POST)
            {
                json = client.DownloadString(url);
            }
            else if (args != null && method.HasValue && method == RequestMethod.POST)
            {
                client.Headers.Add(HttpRequestHeader.ContentType, "application/json");
                string json_args = JsonUtility.ToJson(args);
                json = client.UploadString(url, (method == RequestMethod.POST) ? "POST" : "GET", json_args);
            }

            EnqueueAction(serverCallback, clientCallback, true, json);
        }
    }

    void EnqueueAction<T>(ServerResponseCallback<T> serverCallback, ClientResponseCallback<T> clientCallback, bool success, string json)
    {
        lock (queue)
        {
            queue.Enqueue(() =>
            {
#if UNITY_EDITOR
                Debug.LogFormat("Response [Json = {0}]", json);
#endif
                serverCallback(true, json, clientCallback);
            });
        }
    }

    void EnqueueAction(ResponseCallback callback, bool success, string json, long serverTimeInSeconds = 0, long expirationTime = 0)
    {
        lock (queue)
        {
            queue.Enqueue(() =>
            {
#if UNITY_EDITOR
                Debug.LogFormat("Response [Json = {0}]", json);
#endif
                callback(true, json);
            });
        }
    }
    #endregion

}
