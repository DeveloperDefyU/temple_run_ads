﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Threading;
using UnityEngine;

public class WebApiClient : MonoBehaviour
{
    [System.NonSerialized]
    public string domain = @"http://ads.defyu.com";
    public string baseUrl { get { return string.Format(@"{0}/api", domain); } }

    private static WebApiClient _instance;
    private static WebApiClient instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<WebApiClient>();

                if (_instance == null)
                {
                    GameObject go = new GameObject();
                    go.name = "WebApiClient";
                    _instance = go.AddComponent<WebApiClient>();
                }
            }

            return _instance;
        }
    }

    public delegate void ClientResponseCallback<T>(bool success, T entity);
    delegate void ServerResponseCallback<T>(bool success, string json, ClientResponseCallback<T> clientCallback);
    public delegate void ClientResponse<T>(bool success, T entity);
    delegate void ResponseCallback(bool success, string json);

    static List<Action> ToDoList = new List<Action>();


#if OFUSCATE
    const string COMMAND_IMAGE = "i";
    const string COMMAND_TEXT = "t";
    const string COMMAND_NUMERIC = "n";
    const string COMMAND_CLICK = "nc";
    const string COMMAND_SHOWN = "ns";
    const string COMMAND_TIMESTAMP = "nt";
    const string COMMAND_SESSION_START = "nss";
    const string COMMAND_SESSION_END = "nse";
#else
    const string COMMAND_IMAGE = "image";
    const string COMMAND_TEXT = "text";
    const string COMMAND_NUMERIC = "numeric";
    const string COMMAND_CLICK = "notifyclick";
    const string COMMAND_SHOWN = "notifyshown";
    const string COMMAND_HIDDEN = "notifyhidden";
    const string COMMAND_TIMESTAMP = "notifytimestamp";
    const string COMMAND_SESSION_START = "notifysessionstart";
    const string COMMAND_SESSION_END = "notifysessionend";
#endif

    void Awake()
    {
        DontDestroyOnLoad(this);
        blocked = false;
    }

    void Update()
    {
        TriggerEnqueuedActions();

        if (ToDoList.Count > 0 && !_working)
            NextRequest();
        else if (ToDoList.Count <=0)
            _working = false;
    }

    private void NextRequest()
    {
        _working = true;
        ToDoList[0].Invoke();
    }

    public static void ClearAll()
    {
        instance.blocked = true;
        instance._ClearAll();
    }

    private void _ClearAll()
    {
        queue.Clear();
        ToDoList.Clear();
    }

    public static void Free()
    {
        //ClearAll();
        instance.blocked = false;
    }

    public static void Request_Image(string key, ClientResponseCallback<string> callback)
    {
        
        string url = string.Format("{0}/{1}/{2}", instance.baseUrl, COMMAND_IMAGE, key);
        ToDoList.Add(() => instance.Request(url, instance.ImageServerCallback, callback));
       // instance.Request(url, instance.ImageServerCallback, callback);
    }

    public static void Request_Text(string key, ClientResponseCallback<string> callback)
    {
        string url = string.Format("{0}/{1}/{2}", instance.baseUrl, COMMAND_TEXT, key);
        ToDoList.Add(() => instance.Request(url, instance.TextServerCallback, callback));
     //   instance.Request(url, instance.TextServerCallback, callback);
    }

    public static void Request_Numeric(string key, ClientResponseCallback<float> callback)
    {
        string url = string.Format("{0}/{1}/{2}", instance.baseUrl, COMMAND_NUMERIC, key);
        ToDoList.Add(() => instance.Request(url, instance.NumericServerCallback, callback));
      //  instance.Request(url, instance.NumericServerCallback, callback);
    }

    public static void Request_NotifyClick(string key)
    {
        string url = string.Format("{0}/{1}/{2}", instance.baseUrl, COMMAND_CLICK, key);
        ToDoList.Add(() => instance.Request(url));
       // instance.Request(url);
    }

    public static void Request_NotifyShown(string key)
    {
        string url = string.Format("{0}/{1}/{2}", instance.baseUrl, COMMAND_SHOWN, key);
        ToDoList.Add(() => instance.Request(url));
        //instance.Request(url);
    }

    public static void Request_NotifyHidden(string key, int elapsedTime)
    {
        if (instance.blocked)
            return;
        string url = string.Format("{0}/{1}/{2}/{3}", instance.baseUrl, COMMAND_HIDDEN, key, elapsedTime);
        ToDoList.Add(() => instance.Request(url));
       // instance.Request(url);
    }

    public static void Request_NotifyTimestamp(string key)
    {
        string url = string.Format("{0}/{1}/{2}", instance.baseUrl, COMMAND_TIMESTAMP, key);
        ToDoList.Add(() => instance.Request(url));
       // instance.Request(url);
    }

    public static void Request_NotifySessionStart(string key)
    {
        string url = string.Format("{0}/{1}/{2}", instance.baseUrl, COMMAND_SESSION_START, key);
        ToDoList.Add(() => instance.Request(url));
       // instance.Request(url);
    }

    public static void Request_NotifySessionEnd(string key)
    {
        string url = string.Format("{0}/{1}/{2}", instance.baseUrl, COMMAND_SESSION_END, key);
        ToDoList.Add(() => instance.Request(url));
       // instance.Request(url);
    }

    void ImageServerCallback(bool success, string json, ClientResponseCallback<string> clientCallback)
    {
        if (clientCallback != null)
        {
            var entity = JsonUtility.FromJson<Response>(json);
            clientCallback(success, entity.Image);
        }
    }

    void TextServerCallback(bool success, string json, ClientResponseCallback<string> clientCallback)
    {
        if (clientCallback != null)
        {
            var entity = JsonUtility.FromJson<Response>(json);
            clientCallback(success, entity.Text);
        }
    }

    void NumericServerCallback(bool success, string json, ClientResponseCallback<float> clientCallback)
    {
        if (clientCallback != null)
        {
            var entity = JsonUtility.FromJson<Response>(json);
            clientCallback(success, entity.Numeric);
        }
    }

    #region Thread

    delegate void QueueAction();
    static Queue<QueueAction> queue = new Queue<QueueAction>();
    private bool _working;
    private bool blocked;

    enum RequestMethod
    {
        GET,
        POST
    }

    void TriggerEnqueuedActions()
    {
        if (queue.Count > 0)
        {
            while (queue.Count > 0)
            {
                QueueAction action = queue.Dequeue();
                action();
            }
        }
    }

    void Request(string url)
    {
#if UNITY_EDITOR
        Debug.LogFormat("[Url = {0}]", url);
#endif
        Thread t;
        t = new Thread(() => ThreadedRequest(url));
        t.Start();
    }

    void Request<T>(string url, ServerResponseCallback<T> serverCallback, ClientResponseCallback<T> clientCallback, object args = null, RequestMethod? method = null, bool addRequestTimeout = true, bool sendRequestEvent = true)
    {
#if UNITY_EDITOR
        Debug.LogFormat("[Url = {0}]", url);
#endif
        Thread t;
        t = new Thread(() => ThreadedRequest(url, serverCallback, clientCallback, args, method));

        t.Start();
    }

    void ThreadedRequest(string url)
    {
        using (WebClient client = new WebClient())
        {
            client.Encoding = System.Text.Encoding.UTF8;

            string json = "{}";
            json = client.DownloadString(url);
        }
    }

    void ThreadedRequest<T>(string url, ServerResponseCallback<T> serverCallback, ClientResponseCallback<T> clientCallback, object args = null, RequestMethod? method = null)
    {
        using (WebClient client = new WebClient())
        {
           
            client.Encoding = System.Text.Encoding.UTF8;

            string json = "{}";
            if (args == null || !method.HasValue || method != RequestMethod.POST)
            {
                //System.Random rnd = new System.Random(int.Parse(System.DateTime.Now.Ticks.ToString().Substring(System.DateTime.Now.Ticks.ToString().Length - 3, 3)));
              //  System.Threading.Thread.Sleep( 1000);
                json = client.DownloadString(url);
            }
            else if (args != null && method.HasValue && method == RequestMethod.POST)
            {
                client.Headers.Add(HttpRequestHeader.ContentType, "application/json");
                string json_args = JsonUtility.ToJson(args);
                json = client.UploadString(url, (method == RequestMethod.POST) ? "POST" : "GET", json_args);
            }

            EnqueueAction(serverCallback, clientCallback, true, json);
          
        }
    }
 

    void EnqueueAction<T>(ServerResponseCallback<T> serverCallback, ClientResponseCallback<T> clientCallback, bool success, string json)
    {
        lock (queue)
        {
            queue.Enqueue(() =>
            {
#if UNITY_EDITOR
                Debug.LogFormat("Response [Json = {0}]", json);
#endif
                serverCallback(true, json, clientCallback);
                CheckIfMoreToDo();
            });
        }
    }

    private void CheckIfMoreToDo()
    {
        ToDoList.RemoveAt(0);
        if (ToDoList.Count > 0)
            NextRequest();
        else
            _working = false;
    }

    void EnqueueAction(ResponseCallback callback, bool success, string json, long serverTimeInSeconds = 0, long expirationTime = 0)
    {
        lock (queue)
        {
            queue.Enqueue(() =>
            {
#if UNITY_EDITOR
                Debug.LogFormat("Response [Json = {0}]", json);
#endif
                callback(true, json);
            });
        }
    }
    #endregion

}
