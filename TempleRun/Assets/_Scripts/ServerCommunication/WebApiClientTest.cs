﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WebApiClientTest : MonoBehaviour
{
    void Start()
    {
        WebApiClient.Request_Image("1", Request_ImageCallback);
        WebApiClient.Request_Text("2", Request_TextCallback);
        WebApiClient.Request_Numeric("3", Request_OptionCallback);
    }

    private void Request_ImageCallback(bool success, string entity)
    {
        Debug.Log("image url: " + entity);
    }

    private void Request_TextCallback(bool success, string entity)
    {
        Debug.Log("text: " + entity);
    }

    private void Request_OptionCallback(bool success, float entity)
    {
        Debug.Log("option: " + entity);
    }
}
