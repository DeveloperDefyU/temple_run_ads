﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

[System.Serializable]
public class Response
{
    public string Key;
    public string Image;
    public string Text;
    public float Numeric;
}