﻿using LlockhamIndustries.Decals;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;

public class GameObjectSelector : MonoBehaviour {

    public List<string> Ids;

    private List<int> _results;

    public Dictionary<string, GameObject[]> OptionObjects;
    // Use this for initialization
    void Start()
    {
        AskForNextResult();

    }

    private void AskForNextResult()
    {
       if(Ids != null && Ids.Count > 0)
        {
            AskForOption(Ids[0]);
            return;
        }

        if (_results != null && _results.Count > 0)
            SetResults();

    }

    private void SetResults()
    {
        CharacterManager.Instance.SetCharaters(_results.ToArray());
    }

    private void AskForOption(string id)
    {
        WebApiClient.Request_Numeric(id,
             (bool success, float result) =>
             {
                 if (success)
                 {
                     ResponseOptionObject(result, id);
                 }

             });


    }

    private void ResponseOptionObject(float result, string id)
    {
        Ids.Remove(id);
        if (_results == null)
            _results = new List<int>();

        if(result >= 0)
            _results.Add((int)result);

        AskForNextResult();
    }


}


