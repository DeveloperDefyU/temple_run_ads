﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class SpriteShopSetter : MonoBehaviour, ISetterString {


    private int nSlots = 6;
    Dictionary<int, string> ToProcess;
    public void SetValue(string v, int i=0)
    {
        if (ToProcess == null)
            ToProcess = new Dictionary<int, string>();

        ToProcess.Add(i, v);

        if(i + 1 == nSlots)
            StartCoroutine(SetAllImages());

    }

    private IEnumerator SetAllImages()
    {
        foreach(var p in ToProcess)
        {
            yield return SetTexture(p.Value, p.Key);
        }
        SgLib.InAppPurchaser.Instance.ResetUIShop();
    }

    private IEnumerator SetTexture(string url, int i)
    {

        var www = UnityWebRequestTexture.GetTexture(url);
        yield return www.Send();

        if (www.isNetworkError)
        {
            // SetDefaultImage(defaultImage);

            www.Dispose();
            www = null;
            yield break;
        }
        else
        {
            try
            {
                var texture = ((DownloadHandlerTexture)www.downloadHandler).texture;

                texture = ((DownloadHandlerTexture)www.downloadHandler).texture;
                Rect rec = new Rect(0, 0, 0, 0);

                rec.width = texture.width;
                rec.height = texture.height;

                SgLib.InAppPurchaser.Instance.coinPacks[i].productImage = Sprite.Create(texture, rec, new Vector2(0.5f, 0.5f)); // , 100;
               

            }
            catch (InvalidOperationException ex)
            {

            }
        }
    }
}
