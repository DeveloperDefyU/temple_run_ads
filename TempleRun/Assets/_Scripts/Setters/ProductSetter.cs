﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.Networking;

public class ProductSetter : MonoBehaviour {

    public void SetPrice(string v, int i)
    {
        GetComponent<SgLib.InAppPurchaser>().coinPacks[i].priceString = v;

    }

    public void SetUrl(string v, int i)
    {
        GetComponent<SgLib.InAppPurchaser>().coinPacks[i].urlString = v;

    }

    public void SetSprite(string v, int i)
    {
        StartCoroutine(SetTexture(v, i));

    }


    private IEnumerator SetTexture(string url, int i)
    {

        var www = UnityWebRequestTexture.GetTexture(url);
        yield return www.Send();

        if (www.isNetworkError)
        {
            // SetDefaultImage(defaultImage);

            www.Dispose();
            www = null;
            yield break;
        }
        else
        {
            try
            {
                var texture = ((DownloadHandlerTexture)www.downloadHandler).texture;

                texture = ((DownloadHandlerTexture)www.downloadHandler).texture;
                Rect rec = new Rect(0, 0, 0, 0);

                rec.width = texture.width;
                rec.height = texture.height;

                SgLib.InAppPurchaser.Instance.coinPacks[i].productImage = Sprite.Create(texture, rec, new Vector2(0.5f, 0.5f)); // , 100;

                if (i == 5) SgLib.InAppPurchaser.Instance.ResetUIShop();
            }
            catch (InvalidOperationException ex)
            {

            }
        }
    }
}
