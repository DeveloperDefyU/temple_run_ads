﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinsForPrice : MonoBehaviour, ISetter {

    string id = "7";
    private void Start()
    {
        AskForOptions(id);
    }

    public void SetValue(int v)
    {
        GetComponent<SgLib.CoinManager>().CoinsForPrize = v;
       
    }

    private void AskForOptions(string id)
    {
        WebApiClient.Request_Numeric(id,
             (bool success, float result) =>
             {
                 if (success)
                 {
                    
                     ResponseOptions(result);
                 }

             });


    }

    private void ResponseOptions(float result)
    {

        SetValue((int)result);

    }
}
