﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnvironmentID : MonoBehaviour, ISetter {

    public GameObject HackCube;
    string id = "8";

    private bool _done;
    private bool _set;

    private void Start()
    {
        WebApiClient.Free();
        if (CharacterManager.Instance.Chosen)
            StartCoroutine(AskDelayed());
    }

    private IEnumerator AskDelayed()
    {
        yield return new WaitForEndOfFrame();
        AskForOptions(id);
    }

    private void Update()
    {
        if(_done && (Personalizer.Instance.AllDone || CharacterManager.Instance.AllLoaded ) && !_set)
        {
            StartGame();
        }
    }

    private void StartGame()
    {
        _set = true;
        GameManager.Instance.DoStart();
        HackCube.SetActive(false);
    }

    public void SetValue(int v)
    {
        if (GetComponent<GameManager>()==null)
            return;
        GetComponent<GameManager>().EnvironmentID = v;
        var envis = FindObjectsOfType<SetEnvironmentActive>();
        foreach(var e in envis)
        {
            e.Set(v);
        }
        _done = true;
      
    }

    private void AskForOptions(string id)
    {
        WebApiClient.Request_Numeric(id,
             (bool success, float result) =>
             {
                 if (success)
                 {

                     ResponseOptions(result);
                 }

             });


    }

    private void ResponseOptions(float result)
    {

        SetValue((int)result);

    }
}
